﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Easy_Bid
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class MyBids : ContentPage
	{
        MasterBid bid = new MasterBid();
        MasterBidManager manager;
        MasterContentManager contentManager;
		public MyBids ()
		{
            manager = MasterBidManager.DefaultManager;
            contentManager = MasterContentManager.DefaultManager;
			InitializeComponent ();
		}

        private void cart_Clicked(object sender, EventArgs e)
        {

        }

        private void content_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var item = e.CurrentSelection.FirstOrDefault() as MasterContent;
            if (item != null)
            {
                Navigation.PushAsync(new DetailContent(item, true), true);
                content.SelectedItem = null;
            }
        }

        private async void ContentPage_Appearing(object sender, EventArgs e)
        {
            DependencyService.Get<ILoadingPageService>().ShowLoadingPage();
            ObservableCollection<MasterBid> bids = await manager.GetBidder(App.username);
            List<MasterContent> contents = new List<MasterContent>();
            foreach(var bid in bids)
            {
                ObservableCollection<MasterContent> items = await contentManager.GetContentbyId(bid.idContent);
                var item = items.FirstOrDefault();
                item.price = bid.price;
                if (item.status == 0)
                {
                    item.statusStr = "Bid belum selesai";
                }
                else
                {
                    if (bid.status == 0)
                    {
                        item.statusStr = "Anda kalah bid";
                    }
                    else
                    {
                        item.statusStr = "Anda menang bid";
                    }
                }
                contents.Add(item);
            }
            content.ItemsSource = contents;
            DependencyService.Get<ILoadingPageService>().HideLoadingPage();
        }

        private async void back_Clicked(object sender, EventArgs e)
        {
            await Navigation.PopAsync(true);
        }
    }
}