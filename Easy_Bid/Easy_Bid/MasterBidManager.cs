﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Threading.Tasks;
using Microsoft.WindowsAzure.MobileServices;

namespace Easy_Bid
{
    public partial class MasterBidManager
    {
        static MasterBidManager defaultInstance = new MasterBidManager();
        MobileServiceClient client;

        IMobileServiceTable<MasterBid> ContentTable;
        const string offlineDbPath = @"localstore.db";

        private MasterBidManager()
        {
            this.client = new MobileServiceClient(@"https://cargoapdev.azurewebsites.net");
            //var store = new MobileServiceSQLiteStore("localstore.db");
            //store.DefineTable<content>();
            this.ContentTable = client.GetTable<MasterBid>();
        }

        public static MasterBidManager DefaultManager
        {
            get
            {
                return defaultInstance;
            }
            private set
            {
                defaultInstance = value;
            }
        }

        public MobileServiceClient CurrentClient
        {
            get { return client; }
        }

        public bool IsOfflineEnabled
        {
            get { return ContentTable is Microsoft.WindowsAzure.MobileServices.Sync.IMobileServiceSyncTable<MasterBid>; }
        }

        public async Task<ObservableCollection<MasterBid>> GetHighestBidbyId(string id)
        {
            try
            {
                IEnumerable<MasterBid> items = await ContentTable
                    .Where(bid => bid.deleted == false)
                    .Where(bid => bid.idContent == id)
                    .OrderByDescending(bid => bid.price)
                    .ToEnumerableAsync();
                return new ObservableCollection<MasterBid>(items);
            }
            catch (MobileServiceInvalidOperationException msioe)
            {
                Debug.WriteLine(@"Invalid sync operation: {0}", msioe.Message);
            }
            catch (Exception e)
            {
                Debug.WriteLine(@"Sync error: {0}", e.Message);
            }
            return null;
        }

        public async Task<ObservableCollection<MasterBid>> GetBidder(string username)
        {
            try
            {
                IEnumerable<MasterBid> items = await ContentTable
                    .Where(bid => bid.deleted == false)
                    .Where(bid => bid.bidder == username)
                    .ToEnumerableAsync();
                return new ObservableCollection<MasterBid>(items);
            }
            catch (MobileServiceInvalidOperationException msioe)
            {
                Debug.WriteLine(@"Invalid sync operation: {0}", msioe.Message);
            }
            catch (Exception e)
            {
                Debug.WriteLine(@"Sync error: {0}", e.Message);
            }
            return null;
        }

        public async Task SaveBidAsync(MasterBid item)
        {
            if (item.id == null)
            {
                await ContentTable.InsertAsync(item);
            }
            else
            {
                await ContentTable.UpdateAsync(item);
            }
        }
    }
}
