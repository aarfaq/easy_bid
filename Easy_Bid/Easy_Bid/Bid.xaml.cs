﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Easy_Bid
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Bid : ContentPage
	{
        MasterContent content = new MasterContent();
        MasterBid masterBid = new MasterBid();
        MasterBidManager manager;
        UInt64 price = 0;
        public Bid (MasterContent master)
		{
            manager = MasterBidManager.DefaultManager;
            CultureInfo.CurrentCulture = CultureInfo.CreateSpecificCulture("id-ID");

            InitializeComponent ();

            content = master;

            contentView.BindingContext = content;

            sk.Text = "S&K : bidder wajib setor biaya bid 10% dari harga barang, bila bidder kalah uang akan dikembalikan 100%.";

        }

        private void menu_Clicked(object sender, EventArgs e)
        {
            Navigation.PopAsync();
        }

        private void cart_Clicked(object sender, EventArgs e)
        {

        }

        private void clear_Clicked(object sender, EventArgs e)
        {

        }

        private async void bid_Clicked(object sender, EventArgs e)
        {
            if(!string.IsNullOrWhiteSpace(bidPrice.Text))
            {
                if (price > content.price)
                {
                    DependencyService.Get<ILoadingPageService>().ShowLoadingPage();
                    masterBid.bidder = App.username;
                    masterBid.seller = content.seller;
                    masterBid.bidDate = DateTime.Now;
                    masterBid.price = price;
                    masterBid.idContent = content.id;
                    await manager.SaveBidAsync(masterBid);
                    await Navigation.PopToRootAsync();
                    DependencyService.Get<ILoadingPageService>().HideLoadingPage();
                }
                else
                {
                    await DisplayAlert("", "Harga bid harus lebih tinggi", "OK");
                }
            }
            else
            {
                bidPrice.Focus();
            }
        }

        private void bidPrice_Focused(object sender, FocusEventArgs e)
        {
            if (price == 0)
            {
                bidPrice.Text = string.Empty;
            }
            else
            {
                bidPrice.Text = price.ToString();
            }
        }

        private void bidPrice_Completed(object sender, EventArgs e)
        {            
            //if (UInt64.TryParse(bidPrice.Text, out price))
            //{
            //    bidPrice.Text = price.ToString("N0", CultureInfo.CreateSpecificCulture("id-ID"));
            //}
            //else
            //{
            //    DisplayAlert("", "Input harus berupa angka", "OK");
            //}
        }

        private void bidPrice_Unfocused(object sender, FocusEventArgs e)
        {
            if (UInt64.TryParse(bidPrice.Text, out price))
            {
                bidPrice.Text = price.ToString("N0", CultureInfo.CreateSpecificCulture("id-ID"));
            }
            else
            {
                DisplayAlert("", "Input harus berupa angka", "OK");
            }
        }
    }
}