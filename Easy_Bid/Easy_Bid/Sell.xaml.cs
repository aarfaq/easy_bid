﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using Plugin.Media;
using Plugin.Media.Abstractions;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Easy_Bid
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Sell : ContentPage
	{
        MasterSell sell = new MasterSell();
        MasterContent content = new MasterContent();
        MasterContentManager manager;
        List<MasterSell> contents = new List<MasterSell>();
        string uniqueName;
        int pos = 0;
        byte[] byteData;
        public Sell ()
		{
			InitializeComponent ();
            manager = MasterContentManager.DefaultManager;
            contents.Add(new MasterSell()
            {
                index = 0,
                photo = "image.png",
                button = "add_photo.png"
            });
            //contents.Add(data1);
            //contents.Add(data2);

            photoView.ItemsSource = contents;
        }
        protected override void OnAppearing()
        {
            base.OnAppearing();
            //photoView.ScrollTo(0);
        }

        private async void Button_Clicked(object sender, EventArgs e)
        {
            if(contents.FirstOrDefault().index == 0)
            {
                if (!CrossMedia.Current.IsPickPhotoSupported)
                {
                    await DisplayAlert("Photos Not Supported", "Permission not granted to photos.", "OK");
                    return;
                }
                var file = await CrossMedia.Current.PickPhotoAsync(new Plugin.Media.Abstractions.PickMediaOptions
                {
                    PhotoSize = PhotoSize.Medium,
                    CompressionQuality = 75
                });

                if (file == null)
                    return;

                Stream stream = file.GetStreamWithImageRotatedForExternalStorage();
                MemoryStream a = new MemoryStream();
                stream.CopyTo(a);
                byteData = a.ToArray();

                contents.Clear();

                contents.Add(new MasterSell()
                {
                    index = 0,
                    photo = ImageSource.FromStream(() => new MemoryStream(byteData)),
                    button = "add_photo.png"
                });
            }
            else
            {
                contents.RemoveAt(pos);
            }            
            photoView.ItemsSource = null;
            photoView.ItemsSource = contents;
        }

        private void photoView_Scrolled(object sender, ItemsViewScrolledEventArgs e)
        {
            pos = e.CenterItemIndex;
        }

        private void ContentPage_Appearing(object sender, EventArgs e)
        {
            //photoView.ScrollTo(0);
            category.SelectedIndex = 0;
            content.category = "mobil";
        }

        private static CloudBlobContainer GetContainer()
        {
            // Parses the connection string for the WindowS Azure Storage Account
            var account = CloudStorageAccount.Parse("DefaultEndpointsProtocol=https;AccountName=cargoapp;AccountKey=s8PWZamGnOMbRfchwZSgVlzwAOGkZIhqFfdL0+z7Mwf0fp1ukDQuxP6fD12XeAjM6taDpfn/LQ2UWKNVI98OgA==;EndpointSuffix=core.windows.net");
            var client = account.CreateCloudBlobClient();

            // Gets a reference to the images container
            var container = client.GetContainerReference("foto-produk-bid");

            return container;
        }

        private async Task UploadImage(Stream imageToUpload)
        {
            if (imageToUpload != null)
            {
                try
                {
                    //var account = CloudStorageAccount.Parse("DefaultEndpointsProtocol=https;AccountName=cargoapp;AccountKey=s8PWZamGnOMbRfchwZSgVlzwAOGkZIhqFfdL0+z7Mwf0fp1ukDQuxP6fD12XeAjM6taDpfn/LQ2UWKNVI98OgA==;EndpointSuffix=core.windows.net");
                    //var blobClient = account.CreateCloudBlobClient();
                    var container = GetContainer();

                    await container.CreateIfNotExistsAsync();

                    uniqueName = Guid.NewGuid().ToString() + ".jpg";
                    var blockBlob = container.GetBlockBlobReference(uniqueName);

                    await blockBlob.UploadFromStreamAsync(imageToUpload);
                }
                catch
                {

                }
            }
            else
            {
                //await DisplayAlert("BUKTI TRANSFER", "PILIH FOTO BUKTI TRANSFER", "OK");
            }
        }

        private async void ImageButton_Clicked(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(title.Text))
            {
                title.Focus();
            }
            else
            {
                if (string.IsNullOrWhiteSpace(bidPrice.Text))
                {
                    bidPrice.Focus();
                }
                else
                {
                    if (string.IsNullOrWhiteSpace(sellPrice.Text))
                    {
                        sellPrice.Focus();
                    }
                    else
                    {
                        if (string.IsNullOrWhiteSpace(description.Text))
                        {
                            description.Focus();
                        }
                        else
                        {
                            if (byteData != null)
                            {
                                Stream stream = new MemoryStream(byteData);
                                DependencyService.Get<ILoadingPageService>().ShowLoadingPage();
                                await UploadImage(stream);
                                content.seller = App.username;
                                content.startBid = DateTime.Now;
                                content.endBid = DateTime.Now.AddMinutes(10);
                                content.title = title.Text;
                                content.price = Convert.ToDouble(bidPrice.Text);
                                content.sellPrice = Convert.ToDouble(sellPrice.Text);
                                content.description = description.Text;
                                content.status = 0;
                                content.photo = "https://cargoapp.blob.core.windows.net/foto-produk-bid/" + uniqueName;
                                await manager.SaveContentAsync(content);
                                await Navigation.PopAsync();
                                await DisplayAlert("", "Berhasil", "OK");
                                DependencyService.Get<ILoadingPageService>().HideLoadingPage();
                            }
                            else
                            {
                                await DisplayAlert(null, "Belum ada foto", "OK");
                                return;
                            }
                        }
                    }
                }

            }
        }

        private void category_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (category.SelectedIndex)
            {
                case 0 : content.category = "mobil"; break;
                case 1: content.category = "motor"; break;
                case 2: content.category = "property"; break;
                case 3: content.category = "mewah"; break;
            }
        }
    }
}