﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Threading.Tasks;
using Microsoft.WindowsAzure.MobileServices;

namespace Easy_Bid
{
    public partial class MasterContentManager
    {
        static MasterContentManager defaultInstance = new MasterContentManager();
        MobileServiceClient client;

        IMobileServiceTable<MasterContent> ContentTable;
        const string offlineDbPath = @"localstore.db";

        private MasterContentManager()
        {
            this.client = new MobileServiceClient(@"https://cargoapdev.azurewebsites.net");
            //var store = new MobileServiceSQLiteStore("localstore.db");
            //store.DefineTable<content>();
            this.ContentTable = client.GetTable<MasterContent>();
        }

        public static MasterContentManager DefaultManager
        {
            get
            {
                return defaultInstance;
            }
            private set
            {
                defaultInstance = value;
            }
        }

        public MobileServiceClient CurrentClient
        {
            get { return client; }
        }

        public bool IsOfflineEnabled
        {
            get { return ContentTable is Microsoft.WindowsAzure.MobileServices.Sync.IMobileServiceSyncTable<MasterContent>; }
        }

        public async Task<ObservableCollection<MasterContent>> GetAllContent()
        {
            try
            {
                IEnumerable<MasterContent> items = await ContentTable
                    .Where(content => content.deleted == false)
                    .Where(content => content.startBid <= DateTime.Now)
                    .Where(content => content.endBid >= DateTime.Now)
                    .Where(content => content.seller != App.username)
                    .ToEnumerableAsync();
                return new ObservableCollection<MasterContent>(items);
            }
            catch (MobileServiceInvalidOperationException msioe)
            {
                Debug.WriteLine(@"Invalid sync operation: {0}", msioe.Message);
            }
            catch (Exception e)
            {
                Debug.WriteLine(@"Sync error: {0}", e.Message);
            }
            return null;
        }

        public async Task<ObservableCollection<MasterContent>> GetContentbyCategory(string category)
        {
            try
            {
                IEnumerable<MasterContent> items = await ContentTable
                    .Where(content => content.deleted == false)
                    .Where(content => content.startBid <= DateTime.Now)
                    .Where(content => content.endBid >= DateTime.Now)
                    .Where(content => content.category.ToLower().Contains(category.ToLower()))
                    .ToEnumerableAsync();
                return new ObservableCollection<MasterContent>(items);
            }
            catch (MobileServiceInvalidOperationException msioe)
            {
                Debug.WriteLine(@"Invalid sync operation: {0}", msioe.Message);
            }
            catch (Exception e)
            {
                Debug.WriteLine(@"Sync error: {0}", e.Message);
            }
            return null;
        }

        public async Task<ObservableCollection<MasterContent>> GetContentbyId(string id)
        {
            try
            {
                IEnumerable<MasterContent> items = await ContentTable
                    .Where(content => content.deleted == false)
                    .Where(content => content.id == id)
                    .ToEnumerableAsync();
                return new ObservableCollection<MasterContent>(items);
            }
            catch (MobileServiceInvalidOperationException msioe)
            {
                Debug.WriteLine(@"Invalid sync operation: {0}", msioe.Message);
            }
            catch (Exception e)
            {
                Debug.WriteLine(@"Sync error: {0}", e.Message);
            }
            return null;
        }

        public async Task<ObservableCollection<MasterContent>> GetContentbySeller(string username)
        {
            try
            {
                IEnumerable<MasterContent> items = await ContentTable
                    .Where(content => content.deleted == false)
                    .Where(content => content.seller == username)
                    .ToEnumerableAsync();
                return new ObservableCollection<MasterContent>(items);
            }
            catch (MobileServiceInvalidOperationException msioe)
            {
                Debug.WriteLine(@"Invalid sync operation: {0}", msioe.Message);
            }
            catch (Exception e)
            {
                Debug.WriteLine(@"Sync error: {0}", e.Message);
            }
            return null;
        }

        public async Task<ObservableCollection<MasterContent>> GetContentbyBidder(string username)
        {
            try
            {
                IEnumerable<MasterContent> items = await ContentTable
                    .Where(content => content.deleted == false)
                    .Where(content => content.bidder == username)
                    .ToEnumerableAsync();
                return new ObservableCollection<MasterContent>(items);
            }
            catch (MobileServiceInvalidOperationException msioe)
            {
                Debug.WriteLine(@"Invalid sync operation: {0}", msioe.Message);
            }
            catch (Exception e)
            {
                Debug.WriteLine(@"Sync error: {0}", e.Message);
            }
            return null;
        }

        public async Task SaveContentAsync(MasterContent item)
        {
            if (item.id == null)
            {
                await ContentTable.InsertAsync(item);
            }
            else
            {
                await ContentTable.UpdateAsync(item);
            }
        }
    }
}
