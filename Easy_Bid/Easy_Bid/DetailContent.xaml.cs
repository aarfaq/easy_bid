﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Easy_Bid
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class DetailContent : ContentPage
	{
        MasterContent content = new MasterContent();
        MasterBid masterBid = new MasterBid();
        MasterBidManager manager;
        MasterContentManager contentManager;
        bool bidorsell;
		public DetailContent (MasterContent master, bool bids = false)
		{
            manager = MasterBidManager.DefaultManager;
            contentManager = MasterContentManager.DefaultManager;

            CultureInfo.CurrentCulture = CultureInfo.CreateSpecificCulture("id-ID");

            InitializeComponent ();

            content = master;

            contentView.BindingContext = content;

            bidorsell = bids;            
		}

        private void menu_Clicked(object sender, EventArgs e)
        {
            Navigation.PopAsync();
        }

        private void cart_Clicked(object sender, EventArgs e)
        {

        }

        private void clear_Clicked(object sender, EventArgs e)
        {

        }

        private void bid_Clicked(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(App.username))
            {
                Navigation.PushAsync(new Bid(content));
            }
            else
            {
                Navigation.PushAsync(new Login());
            }
        }

        private void sell_Clicked(object sender, EventArgs e)
        {

        }

        private async void ContentPage_Appearing(object sender, EventArgs e)
        {
            if (bidorsell)
                button.IsVisible = true;
            else
                button.IsVisible = false;

            if (content.endBid >= DateTime.Now)
            {
                double timeLeft = (content.endBid - DateTime.Now).TotalDays;
                int days = (int)Math.Floor(timeLeft);
                double hoursLeft = (timeLeft - days) * 24;
                int hours = (int)Math.Floor(hoursLeft);
                time.Text = "Waktu tersisa : " + days.ToString() + " hari " + hours.ToString() + " jam ";
                ObservableCollection<MasterBid> contents = await manager.GetHighestBidbyId(content.id);
                if (contents.Count > 0)
                {
                    lastBid.Text = "Bid tertinggi : Rp. " + contents.FirstOrDefault().price.ToString("N0", CultureInfo.CreateSpecificCulture("id-ID"));
                }
                else
                {
                    lastBid.Text = "Belum ada bidder";
                }                
            }
            else
            {
                ObservableCollection<MasterBid> contents = await manager.GetHighestBidbyId(content.id);
                if (contents.Count > 0)
                {
                    masterBid = contents.FirstOrDefault();
                    lastBid.Text = "Bid tertinggi : Rp. " + masterBid.price.ToString("N0", CultureInfo.CreateSpecificCulture("id-ID"));
                    time.Text = "Pemenang bid : " + masterBid.bidder;
                    if (masterBid.status != 1)
                    {
                        masterBid.status = 1;
                        await manager.SaveBidAsync(masterBid);
                    }

                    if (string.IsNullOrWhiteSpace(content.bidder))
                    {
                        content.bidder = masterBid.bidder;
                        content.price = masterBid.price;
                        await contentManager.SaveContentAsync(content);
                    }

                    if (bidorsell)
                    {
                        pay_btn.IsVisible = true;
                        button.IsVisible = false;
                    }
                }
                else
                {
                    lastBid.Text = "Tidak ada bidder";
                    time.IsVisible = false;
                }
            }
        }

        private void pay_btn_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new Delivery(content));
        }
    }
}