﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Easy_Bid
{
    public class MasterBid
    {
        public string id { get; set; }
        public string idContent { get; set; }
        public DateTime createdAt { get; set; }
        public DateTime updatedAt { get; set; }
        public bool deleted { get; set; }
        public string seller { get; set; }        
        public string bidder { get; set; }
        public DateTime bidDate { get; set; }
        public int status { get; set; }
        public double price { get; set; }
    }
}
