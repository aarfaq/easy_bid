﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.WindowsAzure.MobileServices;

namespace Easy_Bid
{
    class ClientBidManager
    {
        static ClientBidManager defaultInstance = new ClientBidManager();
        MobileServiceClient client;

        IMobileServiceTable<ClientBid> RegTable;
        const string offlineDbPath = @"localstore.db";

        private ClientBidManager()
        {
            this.client = new MobileServiceClient(@"https://cargoapdev.azurewebsites.net");
            //var store = new MobileServiceSQLiteStore("localstore.db");
            //store.DefineTable<ClientReg>();
            this.RegTable = client.GetTable<ClientBid>();
        }

        public static ClientBidManager DefaultManager
        {
            get
            {
                return defaultInstance;
            }
            private set
            {
                defaultInstance = value;
            }
        }

        //public async Task SyncAsync()
        //{
        //	ReadOnlyCollection<MobileServiceTableOperationError> syncErrors = null;

        //	try
        //	{
        //		await this.client.SyncContext.PushAsync();

        //		// The first parameter is a query name that is used internally by the client SDK to implement incremental sync.
        //		// Use a different query name for each unique query in your program.
        //		await this.RegTable.PullAsync("allTodoItems", this.RegTable.CreateQuery());
        //	}
        //	catch (MobileServicePushFailedException exc)
        //	{
        //		if (exc.PushResult != null)
        //		{
        //			syncErrors = exc.PushResult.Errors;
        //		}
        //	}

        // Simple error/conflict handling.
        //	if (syncErrors != null)
        //	{
        //		foreach (var error in syncErrors)
        //		{
        //			if (error.OperationKind == MobileServiceTableOperationKind.Update && error.Result != null)
        //			{
        //				// Update failed, revert to server's copy
        //				await error.CancelAndUpdateItemAsync(error.Result);
        //			}
        //			else
        //			{
        //				// Discard local change
        //				await error.CancelAndDiscardItemAsync();
        //			}

        //			Debug.WriteLine(@"Error executing sync operation. Item: {0} ({1}). Operation discarded.", error.TableName, error.Item["id"]);
        //		}
        //	}
        //}

        public MobileServiceClient CurrentClient
        {
            get { return client; }
        }

        public bool IsOfflineEnabled
        {
            get { return RegTable is Microsoft.WindowsAzure.MobileServices.Sync.IMobileServiceSyncTable<ClientBid>; }
        }
        
        public async Task<ObservableCollection<ClientBid>> GetUserAsync(string user)
        {
            try
            {
                IEnumerable<ClientBid> items = await RegTable
                    .Where(clientReg => clientReg.username == user)
                    .ToEnumerableAsync();
                return new ObservableCollection<ClientBid>(items);
            }
            catch (MobileServiceInvalidOperationException msioe)
            {
                Debug.WriteLine(@"Invalid sync operation: {0}", msioe.Message);
            }
            catch (Exception e)
            {
                Debug.WriteLine(@"Sync error: {0}", e.Message);
            }
            return null;
        }

        public async Task<ObservableCollection<ClientBid>> GetAccountAsync(string user, string pass)
        {
            try
            {
                IEnumerable<ClientBid> items = await RegTable
                    .Where(clientReg => clientReg.username == user)
                    .Where(clientReg => clientReg.password == pass)
                    .ToEnumerableAsync();
                return new ObservableCollection<ClientBid>(items);
            }
            catch (MobileServiceInvalidOperationException msioe)
            {
                Debug.WriteLine(@"Invalid sync operation: {0}", msioe.Message);
            }
            catch (Exception e)
            {
                Debug.WriteLine(@"Sync error: {0}", e.Message);
            }
            return null;
        }

        public async Task SaveTaskAsync(ClientBid item)
        {
            if (item.id == null)
            {
                await RegTable.InsertAsync(item);
            }
            else
            {
                await RegTable.UpdateAsync(item);
            }
        }
    }
}
