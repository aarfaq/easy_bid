﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace Easy_Bid
{
    public interface ILoadingPageService
    {
        void InitLoadingPage(ContentPage loadingIndicatorPage = null);

        void ShowLoadingPage();

        void HideLoadingPage();
    }
}
