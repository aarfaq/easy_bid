﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using System.Collections.ObjectModel;
using Plugin.Media;
using Plugin.Media.Abstractions;

namespace Easy_Bid
{
    [XamlCompilation(XamlCompilationOptions.Compile)]

    public partial class AdsTemplate
    {
        public string ads { get; set; }
        public string dot { get; set; }
    }
    public partial class Home : ContentPage
    {
        MasterContent masterContent = new MasterContent();
        MasterContentManager manager;
        ClientBidManager clientManager;
        ClientBid clientBid = new ClientBid();
        public static double screenwidth { get; set; }
        public static double fullscreenwidth { get; set; }
        public static double adsscreenwidth { get; set; }

        public List<AdsTemplate> ads = new List<AdsTemplate>();
        public List<AdsTemplate> ads1 = new List<AdsTemplate>();

        int idx0 = 0;
        int idx1 = 0;

        int idx01 = 0;
        int idx11 = 0;

        bool scrolling = false;
        bool scrolling1 = false;
        public Home(string category = null)
        {
            CultureInfo.CurrentCulture = CultureInfo.CreateSpecificCulture("id-ID");

            screenwidth = (App.ScreenWidth * 0.4);
            fullscreenwidth = App.ScreenWidth;
            adsscreenwidth = App.ScreenWidth * 0.9;

            InitializeComponent();

            manager = MasterContentManager.DefaultManager;
            clientManager = ClientBidManager.DefaultManager;

            ads.Add(new AdsTemplate()
            {
                ads = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSp8Yvr164DLHpMRnnpud2H49Sx9hGvfTcvLEjSpIvFPi1-pGqBPg",
                dot = "dot1.png"
            });

            ads.Add(new AdsTemplate()
            {
                ads = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTAt-o_JqSpTfv2GRTVKggtGdUdlbQcYjS0tYb9Qr6myAUo8Mc9",
                dot = "dot0.png"
            });

            ads.Add(new AdsTemplate()
            {
                ads = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR3i8Dxcwqi471lfvLz0rQwEM2Ug8fgUY82QO_remwT1qt1QyQS",
                dot = "dot0.png"
            });

            adsView.ItemsSource = ads;
            dotview.ItemsSource = ads;

            ads1 = ads;

            adsView1.ItemsSource = ads1;
            dotview1.ItemsSource = ads1;

            //List<MasterContent> contents = new List<MasterContent>();


            //MasterContent data1 = new MasterContent()
            //{
            //    photo = "https://images.unsplash.com/photo-1523676060187-f55189a71f5e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80",
            //    title = "TITLE",
            //    price = 10000,
            //};

            //MasterContent data2 = new MasterContent()
            //{
            //    photo = "https://images.pexels.com/photos/112460/pexels-photo-112460.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500",
            //    title = "TITLE2",
            //    price = 10000,
            //};

            //MasterContent data3 = new MasterContent()
            //{
            //    photo = "https://cdn.pixabay.com/photo/2012/05/29/00/43/car-49278__340.jpg",
            //    title = "TITLE3",
            //    price = 50000,
            //};

            //MasterContent data4 = new MasterContent()
            //{
            //    photo = "https://images-na.ssl-images-amazon.com/images/I/71%2B1tRw991L._SX355_.jpg",
            //    title = "TITLE",
            //    price = 10000,
            //};

            //MasterContent data5 = new MasterContent()
            //{
            //    photo = "https://www.outdoortoys.co.uk/media/catalog/product/cache/d2cc037f17566da9bacb29decf785134/i/m/img_5830_2.jpg",
            //    title = "TITLE2",
            //    price = 10000,
            //};

            //MasterContent data6 = new MasterContent()
            //{
            //    photo = "https://img-new.cgtrader.com/items/1951241/20eff6be14/motorbike-chopper-3d-model-max-obj-mtl-fbx-wrl-wrz.jpg",
            //    title = "TITLE3",
            //    price = 50000,
            //};

            //MasterContent data7 = new MasterContent()
            //{
            //    photo = "https://business.nab.com.au/wp-content/uploads/2017/07/house-740x530.png",
            //    title = "TITLE",
            //    price = 10000,
            //};

            //MasterContent data8 = new MasterContent()
            //{
            //    photo = "https://goalcharlotte.propertywaresites.com/wp-content/uploads/sites/620/2019/07/3239-Runneymede-large.jpg",
            //    title = "TITLE2",
            //    price = 10000,
            //};

            //MasterContent data9 = new MasterContent()
            //{
            //    photo = "https://images.prop24.com/220243381",
            //    title = "TITLE3",
            //    price = 50000,
            //};

            //MasterContent data10 = new MasterContent()
            //{
            //    photo = "https://data.whicdn.com/images/82237125/large.jpg",
            //    title = "TITLE",
            //    price = 10000,
            //};

            //MasterContent data11 = new MasterContent()
            //{
            //    photo = "https://www.wowamazing.com/wp-content/uploads/2015/08/WOW-Amazing-FI-Luxury.jpg",
            //    title = "TITLE2",
            //    price = 10000,
            //};

            //MasterContent data12 = new MasterContent()
            //{
            //    photo = "https://karatvan.co.id/wp-content/uploads/2019/07/cover-17.jpg",
            //    title = "TITLE3",
            //    price = 50000,
            //};

            //if (category == "car")
            //{
            //    contents.Add(data1);
            //    contents.Add(data2);
            //    contents.Add(data3);
            //}
            //else if (category == "mcycle")
            //{
            //    contents.Add(data4);
            //    contents.Add(data5);
            //    contents.Add(data6);
            //}
            //else if (category == "property")
            //{
            //    contents.Add(data7);
            //    contents.Add(data8);
            //    contents.Add(data9);
            //}
            //else if (category == "lux")
            //{
            //    contents.Add(data10);
            //    contents.Add(data11);
            //    contents.Add(data12);
            //}
            //else
            //{
            //    contents.Add(data1);
            //    contents.Add(data2);
            //    contents.Add(data3);
            //    contents.Add(data4);
            //    contents.Add(data5);
            //    contents.Add(data6);
            //    contents.Add(data7);
            //    contents.Add(data8);
            //    contents.Add(data9);
            //    contents.Add(data10);
            //    contents.Add(data11);
            //    contents.Add(data12);
            //}

            //content.ItemsSource = contents;
        }

        private void menu_Clicked(object sender, EventArgs e)
        {
            (Application.Current.MainPage.Navigation.NavigationStack.First() as MasterDetailPage).IsPresented = true;
        }

        private void cart_Clicked(object sender, EventArgs e)
        {

        }

        private void clear_Clicked(object sender, EventArgs e)
        {
            search.Text = string.Empty;
            search.Focus();
        }

        private void content_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var item = e.CurrentSelection.FirstOrDefault() as MasterContent;
            if (item != null)
            {
                Navigation.PushAsync(new DetailContent(item, true), true);
                content.SelectedItem = null;
            }
        }
        private void ImageButton_Clicked(object sender, EventArgs e)
        {
            Navigation.InsertPageBefore(new Home("car"), this);
            Navigation.PopToRootAsync();
        }

        private void ImageButton_Clicked_1(object sender, EventArgs e)
        {
            Navigation.InsertPageBefore(new Home("mcycle"), this);
            Navigation.PopToRootAsync();
        }

        private void ImageButton_Clicked_2(object sender, EventArgs e)
        {
            Navigation.InsertPageBefore(new Home("property"), this);
            Navigation.PopToRootAsync();
        }

        private void ImageButton_Clicked_3(object sender, EventArgs e)
        {
            Navigation.InsertPageBefore(new Home("lux"), this);
            Navigation.PopToRootAsync();
        }

        private async void ContentPage_Appearing(object sender, EventArgs e)
        {
            
        }

        private void adsView_Scrolled(object sender, ItemsViewScrolledEventArgs e)
        {
            idx1 = e.CenterItemIndex;
            int x = (int)e.HorizontalOffset;
            int y = idx1 * (int)(adsView.Width * 2);

            if (Math.Abs(x - y) <= 50)
            {
                scrolling = false;
                ads.ElementAt(idx0).dot = "dot0.png";
                ads.ElementAt(idx1).dot = "dot1.png";
                dotview.ItemsSource = null;
                dotview.ItemsSource = ads;
                idx0 = idx1;
            }

            if (idx0 != idx1)
            {
                if (!scrolling)
                {
                    scrolling = true;
                    ads.ElementAt(idx0).dot = "dot0.png";
                    dotview.ItemsSource = null;
                    dotview.ItemsSource = ads;
                }
            }
        }

        private void adsView1_Scrolled(object sender, ItemsViewScrolledEventArgs e)
        {
            idx11 = e.CenterItemIndex;
            int x = (int)e.HorizontalOffset;
            int y = idx11 * (int)(adsView1.Width * 2);

            if (Math.Abs(x - y) <= 50)
            {
                scrolling1 = false;
                ads1.ElementAt(idx01).dot = "dot0.png";
                ads1.ElementAt(idx11).dot = "dot1.png";
                dotview1.ItemsSource = null;
                dotview1.ItemsSource = ads1;
                idx01 = idx11;
            }

            if (idx01 != idx11)
            {
                if (!scrolling1)
                {
                    scrolling1 = true;
                    ads1.ElementAt(idx01).dot = "dot0.png";
                    dotview1.ItemsSource = null;
                    dotview1.ItemsSource = ads1;
                }
            }
        }

        private void home_Clicked(object sender, EventArgs e)
        {

        }

        private void category_Clicked(object sender, EventArgs e)
        {

        }

        private void mybids_Clicked(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(App.username))
            {
                Navigation.PushAsync(new MyBids());
            }
            else
            {
                Navigation.PushAsync(new Login());
            }
        }

        private void fav_Clicked(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(App.username))
            {
                Navigation.PushAsync(new MySell());
            }
            else
            {
                Navigation.PushAsync(new Login());
            }
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();

            adsView.ScrollTo(0);
            DependencyService.Get<ILoadingPageService>().ShowLoadingPage();
            ObservableCollection<MasterContent> contents = await manager.GetAllContent();
            content.ItemsSource = contents;
            if (!string.IsNullOrWhiteSpace(App.username))
            {
                ObservableCollection<ClientBid> items = await clientManager.GetAccountAsync(App.username, App.password);

                if (items.Count == 1)
                {
                    App.client = items.FirstOrDefault();
                    clientBid = App.client;
                }
                else
                {
                    App.username = string.Empty;
                    App.password = string.Empty;
                }
            }
            else
            {
                clientBid.name = "Belum Masuk";
            }

            account.BindingContext = clientBid;
            DependencyService.Get<ILoadingPageService>().HideLoadingPage();
        }
    }
}