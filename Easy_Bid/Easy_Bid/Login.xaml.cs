﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Easy_Bid
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Login : ContentPage
	{
        ClientBidManager manager;
        public Login (string username = null)
        {
            manager = ClientBidManager.DefaultManager;

            InitializeComponent ();

            if (username != null) ;
            Username.Text = username;
		}

        private async void login_Clicked(object sender, EventArgs e)
        {
            if (Username.Text == null)
            {
                await DisplayAlert("", "Username harus diisi", "OK");
                Username.Focus();
            }
            else
            {
                if (Password.Text == null)
                {
                    await DisplayAlert("", "Password harus diisi", "OK");
                    Password.Focus();
                }
                else
                {
                    await RefreshItems(Username.Text.ToLower(), Password.Text);
                }
            }
        }

        private void registrasi_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new Registration());
        }

        private async Task RefreshItems(string user, string pass)
        {
            ObservableCollection<ClientBid> items;
            //using (var scope = new ActivityIndicatorScope(syncIndicator, true))
            //{
            DependencyService.Get<ILoadingPageService>().ShowLoadingPage();
            items = await manager.GetAccountAsync(user, pass);
            //}
            if (items.Count == 1)
            {
                var item = items.FirstOrDefault();

                await SecureStorage.SetAsync("user", user);
                await SecureStorage.SetAsync("pass", pass);

                App.username = user;
                App.password = pass;

                App.client = item;

                Navigation.InsertPageBefore(new MainPage(), Application.Current.MainPage.Navigation.NavigationStack.First());
                await Navigation.PopToRootAsync();

                await Navigation.PopAsync();              
                
            }
            else
            {
                await DisplayAlert("LOGIN", "Username/paswword salah", "OK");
            }

            DependencyService.Get<ILoadingPageService>().HideLoadingPage();
        }
    }
}