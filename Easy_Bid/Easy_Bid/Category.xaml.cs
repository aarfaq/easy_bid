﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Easy_Bid
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Category : ContentPage
	{
		public Category ()
		{
			InitializeComponent ();
		}

        private void menu_Clicked(object sender, EventArgs e)
        {
            (Application.Current.MainPage.Navigation.NavigationStack.First() as MasterDetailPage).IsPresented = true;
        }

        private void cart_Clicked(object sender, EventArgs e)
        {

        }

        private void ImageButton_Clicked(object sender, EventArgs e)
        {
            Navigation.InsertPageBefore(new Home("car"), this);
            Navigation.PopToRootAsync();
        }

        private void ImageButton_Clicked_1(object sender, EventArgs e)
        {
            Navigation.InsertPageBefore(new Home("mcycle"), this);
            Navigation.PopToRootAsync();
        }

        private void ImageButton_Clicked_2(object sender, EventArgs e)
        {
            Navigation.InsertPageBefore(new Home("property"), this);
            Navigation.PopToRootAsync();
        }

        private void ImageButton_Clicked_3(object sender, EventArgs e)
        {
            Navigation.InsertPageBefore(new Home("lux"), this);
            Navigation.PopToRootAsync();
        }
    }
}