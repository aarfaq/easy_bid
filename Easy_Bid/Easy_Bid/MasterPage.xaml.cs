﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Easy_Bid
{
    public partial class MasterPage : ContentPage
    {
        ClientBid client = new ClientBid();

        ClientBidManager manager;
        public MasterPage()
        {
            manager = ClientBidManager.DefaultManager;

            InitializeComponent();                        
        }

        private void ContentPage_Appearing(object sender, System.EventArgs e)
        {
            
        }

        private void button_Clicked(object sender, System.EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(App.username))
            {
                Navigation.PushAsync(new Login());
            }
        }
    }
}