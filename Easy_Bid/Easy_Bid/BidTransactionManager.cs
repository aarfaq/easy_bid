﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Threading.Tasks;
using Microsoft.WindowsAzure.MobileServices;

namespace Easy_Bid
{
    class BidTransactionManager
    {
        static BidTransactionManager defaultInstance = new BidTransactionManager();
        MobileServiceClient client;

        IMobileServiceTable<BidTransaction> ContentTable;
        const string offlineDbPath = @"localstore.db";

        private BidTransactionManager()
        {
            this.client = new MobileServiceClient(@"https://cargoapdev.azurewebsites.net");
            //var store = new MobileServiceSQLiteStore("localstore.db");
            //store.DefineTable<content>();
            this.ContentTable = client.GetTable<BidTransaction>();
        }

        public static BidTransactionManager DefaultManager
        {
            get
            {
                return defaultInstance;
            }
            private set
            {
                defaultInstance = value;
            }
        }

        public MobileServiceClient CurrentClient
        {
            get { return client; }
        }

        public bool IsOfflineEnabled
        {
            get { return ContentTable is Microsoft.WindowsAzure.MobileServices.Sync.IMobileServiceSyncTable<BidTransaction>; }
        }

        public async Task<ObservableCollection<BidTransaction>> GetTransSeller(string username)
        {
            try
            {
                IEnumerable<BidTransaction> items = await ContentTable
                    .Where(content => content.deleted == false)
                    .Where(content => content.seller == username)
                    .ToEnumerableAsync();
                return new ObservableCollection<BidTransaction>(items);
            }
            catch (MobileServiceInvalidOperationException msioe)
            {
                Debug.WriteLine(@"Invalid sync operation: {0}", msioe.Message);
            }
            catch (Exception e)
            {
                Debug.WriteLine(@"Sync error: {0}", e.Message);
            }
            return null;
        }

        public async Task<ObservableCollection<BidTransaction>> GetTransBidder(string username)
        {
            try
            {
                IEnumerable<BidTransaction> items = await ContentTable
                    .Where(content => content.deleted == false)
                    .Where(content => content.bidder == username)
                    .ToEnumerableAsync();
                return new ObservableCollection<BidTransaction>(items);
            }
            catch (MobileServiceInvalidOperationException msioe)
            {
                Debug.WriteLine(@"Invalid sync operation: {0}", msioe.Message);
            }
            catch (Exception e)
            {
                Debug.WriteLine(@"Sync error: {0}", e.Message);
            }
            return null;
        }

        public async Task SaveTransAsync(BidTransaction item)
        {
            if (item.id == null)
            {
                await ContentTable.InsertAsync(item);
            }
            else
            {
                await ContentTable.UpdateAsync(item);
            }
        }
    }
}
