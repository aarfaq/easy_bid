﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Easy_Bid
{
    public class ClientBid
    {
        public string id { get; set; }
        public DateTime createdAt { get; set; }
        public DateTime updatedAt { get; set; }
        public bool deleted { get; set; }
        public string name { get; set; }
        public string username { get; set; }
        public string password { get; set; }
        public string email { get; set; }
        public string phone { get; set; }
        public string status { get; set; }
        public string photo { get; set; }
    }
}
