using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Xamarin.Essentials;

[assembly: XamlCompilation (XamlCompilationOptions.Compile)]
namespace Easy_Bid
{
	public partial class App : Application
	{
        public static double ScreenHeight;
        public static double ScreenWidth;
        public static string username;
        public static string password;
        public static ClientBid client = new ClientBid();
        public App ()
		{
			InitializeComponent();

            MainPage = new NavigationPage(new Home());
        }

		protected override async void OnStart ()
		{
            //Handle when your app starts
            try
            {
                username = await SecureStorage.GetAsync("user");
                password = await SecureStorage.GetAsync("pass");
            }
            catch (Exception ex)
            {
                username = string.Empty;
                password = string.Empty;
                await SecureStorage.SetAsync("user", username);
                await SecureStorage.SetAsync("pass", password);
            }
        }

		protected override void OnSleep ()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume ()
		{
			// Handle when your app resumes
		}
	}
}
