﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Easy_Bid
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class MySell : ContentPage
	{
        MasterBid bid = new MasterBid();
        MasterBidManager manager;
        MasterContentManager contentManager;
        public MySell ()
        {
            manager = MasterBidManager.DefaultManager;
            contentManager = MasterContentManager.DefaultManager;
            InitializeComponent ();
		}

        private async void ContentPage_Appearing(object sender, EventArgs e)
        {
            DependencyService.Get<ILoadingPageService>().ShowLoadingPage();
            ObservableCollection<MasterContent> items = await contentManager.GetContentbySeller(App.username);
            List<MasterContent> contents = new List<MasterContent>();
            foreach (var item in items)
            {
                if(item.endBid < DateTime.Now)
                {
                    if (item.status == 0)
                    {
                        item.status = 1;
                        await contentManager.SaveContentAsync(item);
                    }
                    item.statusStr = "Bid sudah selesai";
                }
                else
                {
                    item.statusStr = "Bid belum selesai";
                }
                contents.Add(item);
            }
            content.ItemsSource = contents;
            DependencyService.Get<ILoadingPageService>().HideLoadingPage();
        }

        private void back_Clicked(object sender, EventArgs e)
        {
            Navigation.PopAsync(true);
        }

        private void cart_Clicked(object sender, EventArgs e)
        {

        }

        private void content_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var item = e.CurrentSelection.FirstOrDefault() as MasterContent;
            if (item != null)
            {
                Navigation.PushAsync(new DetailContent(item, false), true);
                content.SelectedItem = null;
            }
        }

        private void add_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new Sell());
        }
    }
}