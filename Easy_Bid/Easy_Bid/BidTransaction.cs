﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Easy_Bid
{
    class BidTransaction
    {
        public string id { get; set; }
        public DateTime createdAt { get; set; }
        public DateTime updatedAt { get; set; }
        public bool deleted { get; set; }
        public int status { get; set; }
        public string idContent { get; set; }
        public string address { get; set; }
        public string recipient { get; set; }
        public string phone { get; set; }
        public string courier { get; set; }
        public string seller { get; set; }
        public string bidder { get; set; }
    }
}
