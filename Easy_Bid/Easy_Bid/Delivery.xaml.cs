﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Easy_Bid
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Delivery : ContentPage
	{
        MasterContent content = new MasterContent();
        BidTransaction transaction = new BidTransaction();
        BidTransactionManager transactionManager;
        MasterRecipient masterRecipient = new MasterRecipient();
        public Delivery (MasterContent master)
		{
            transactionManager = BidTransactionManager.DefaultManager;

            InitializeComponent();

            content = master;

            contentview.BindingContext = content;

            masterRecipient.name = "Nama Penerima";
            masterRecipient.address = "Jl. A. Yani 10, Surabaya";
            masterRecipient.phone = "08263111551";

            recipient.BindingContext = masterRecipient;			
		}

        private void ContentPage_Appearing(object sender, EventArgs e)
        {

        }

        private async void ImageButton_Clicked(object sender, EventArgs e)
        {
            transaction.seller = content.seller;
            transaction.bidder = content.bidder;
            transaction.courier = "JNE";
            transaction.recipient = masterRecipient.name;
            transaction.address = masterRecipient.address;
            transaction.phone = masterRecipient.phone;
            transaction.status = 0;
            transaction.idContent = content.id;

            await transactionManager.SaveTransAsync(transaction);
            await Navigation.PopToRootAsync(true);
        }
    }
}