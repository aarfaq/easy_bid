﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Easy_Bid
{
    public class MasterContent
    {
        public string id { get; set; }
        public DateTime createdAt { get; set; }
        public DateTime updatedAt { get; set; }
        public bool deleted { get; set; }
        public string title { get; set; }
        public double price { get; set; }
        public DateTime startBid { get; set; }
        public DateTime endBid { get; set; }
        public int status { get; set; }
        public string statusStr { get; set; }
        public string photo { get; set; }
        public string category { get; set; }
        public double sellPrice { get; set; }
        public string description { get; set; }
        public string seller { get; set; }
        public string bidder { get; set; }
    }
}
