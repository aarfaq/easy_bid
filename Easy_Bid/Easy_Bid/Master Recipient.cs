﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Easy_Bid
{
    public class MasterRecipient
    {
        public string name { get; set; }
        public string address { get; set; }
        public string phone { get; set; }
        public string courier { get; set; }
    }
}
