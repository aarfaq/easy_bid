﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Easy_Bid
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Registration : ContentPage
	{
        ClientBidManager manager;
        public Action CustomBackButtonAction { get; set; }
        public Registration ()
		{
            manager = ClientBidManager.DefaultManager;

			InitializeComponent ();

            this.CustomBackButtonAction = async () =>
            {
                var result = await this.DisplayAlert(null,
                    "Batal registrasi?",
                    "Ya", "Tidak");

                if (result)
                {
                    await Navigation.PopModalAsync(true);
                }
            };
        }

        private async void registrasi_Clicked(object sender, EventArgs e)
        {
            if (name.Text != null && username.Text != null && password.Text != null && re_password.Text != null && email.Text != null && phone.Text != null)
            {
                if (password.Text.Length >= 6)
                {
                    DependencyService.Get<ILoadingPageService>().ShowLoadingPage();
                    ObservableCollection<ClientBid> items;
                    //using (var scope = new ActivityIndicatorScope(syncIndicator, true))
                    //{
                    items = await manager.GetUserAsync(username.Text.ToLower());

                    if (items.Count == 1)
                    {
                        await DisplayAlert(null, "Username sudah dipakai", "OK");
                        username.Focus();
                    }
                    else
                    {
                        if (re_password.Text == password.Text)
                        {
                            var todo = new ClientBid { name = name.Text, username = username.Text.ToLower(), password = password.Text, email = email.Text, phone = phone.Text};
                            await manager.SaveTaskAsync(todo);
                            await DisplayAlert("Registrasi", "Berhasil Registrasi", "OK");
                            await Navigation.PopAsync();
                        }
                        else
                        {
                            await DisplayAlert(null, "Password tidak sama", "OK");
                            password.Focus();
                        }
                    }
                    DependencyService.Get<ILoadingPageService>().HideLoadingPage();
                    //}
                }
                else
                {
                    await DisplayAlert(null, "Password minimal 6 karakter", "OK");
                    password.Focus();
                }

            }
            else
            {
                await DisplayAlert(null, "Form tidak lengkap", "OK");
            }
        }
    }
}