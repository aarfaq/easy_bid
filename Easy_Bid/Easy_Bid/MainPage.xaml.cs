﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Easy_Bid
{
    public partial class MainPage : MasterDetailPage
    {
        public int screenheight { get; set; }
        ClientBidManager manager;        
        public MainPage()
        {
            manager = ClientBidManager.DefaultManager;
            InitializeComponent();

            screenheight = (int)(App.ScreenHeight / 13);

            masterPage.listView.RowHeight = screenheight;
            masterPage.listView1.RowHeight = screenheight;

            masterPage.listView.ItemSelected += OnItemSelected;
            masterPage.listView1.ItemSelected += OnItemSelected1;

            masterPage.button.Clicked += masterButtonClicked;

            if (Device.RuntimePlatform == Device.UWP)
            {
                MasterBehavior = MasterBehavior.Popover;
            }

            Detail = new NavigationPage(new Home());
        }

        private void masterButtonClicked(object sender, EventArgs e)
        {
            
        }

        void OnItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            var item = e.SelectedItem as MasterPageItem;
            if (item != null)
            {
                //Detail = new NavigationPage((Page)Activator.CreateInstance(item.TargetType));
                if (item.Title == "Kategori")
                    Detail = new NavigationPage(new Category());
                else if (item.Title == "Jual")
                {
                    if (!string.IsNullOrWhiteSpace(App.username))
                    {
                        Navigation.PushModalAsync(new Sell());
                    }
                    else
                    {
                        Navigation.PushAsync(new Login());
                    }
                }

                else
                    Detail = new NavigationPage(new Home(null));

                masterPage.listView.SelectedItem = null;
                IsPresented = false;
            }
        }
        void OnItemSelected1(object sender, SelectedItemChangedEventArgs e)
        {
            var item = e.SelectedItem as MasterPageItem;
            if (item != null)
            {
                //Detail = new NavigationPage((Page)Activator.CreateInstance(item.TargetType));
                Detail = new NavigationPage(new Home(null));
                masterPage.listView1.SelectedItem = null;
                IsPresented = false;
            }
        }

        private async void MasterDetailPage_Appearing(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(App.username))
            {
                DependencyService.Get<ILoadingPageService>().ShowLoadingPage();

                ObservableCollection<ClientBid> items = await manager.GetAccountAsync(App.username, App.password);

                if (items.Count == 1)
                {
                    App.client = items.FirstOrDefault();
                    masterPage.button.Source = "memberregs.png";
                }
                else
                {
                    App.client.photo = "photo.png";
                    masterPage.button.Source = "login.png";
                    App.username = string.Empty;
                    App.password = string.Empty;
                }

                DependencyService.Get<ILoadingPageService>().HideLoadingPage();
            }
            else
            {
                masterPage.button.Source = "login.png";
            }

            if (string.IsNullOrWhiteSpace(App.client.photo))
                App.client.photo = "photo.png";

            masterPage.account.BindingContext = App.client;
        }
    }
}