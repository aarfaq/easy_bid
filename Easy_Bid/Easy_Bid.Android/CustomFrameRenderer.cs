﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using Easy_Bid;
using Easy_Bid.Droid;
using Android.Graphics.Drawables;
using Android.Graphics;

[assembly: ExportRenderer(typeof(CustomFrame), typeof(CustomFrameRenderer))]
namespace Easy_Bid.Droid
{
    public class CustomFrameRenderer : FrameRenderer
    {
        Context _context;
        public CustomFrameRenderer(Context context) : base(context)
        {
            _context = context;
        }
        protected override void OnElementChanged(ElementChangedEventArgs<Frame> e)
        {
            base.OnElementChanged(e);

            if (e.NewElement != null && e.OldElement == null)
            {
                var customE = e.NewElement as CustomFrame;

                GradientDrawable drawable = new GradientDrawable();
                drawable.SetColor(Element.BackgroundColor.ToAndroid());
                drawable.SetCornerRadius(Android.App.Application.Context.ToPixels(Element.CornerRadius));
                drawable.SetGradientType(GradientType.SweepGradient);

                this.SetElevation(customE.ElevationHeight);
                ClipToOutline = Element.IsClippedToBounds;
                this.SetClipChildren(Element.IsClippedToBounds);
                this.SetClipToOutline(Element.IsClippedToBounds);

                //this.SetBackgroundResource(Resource.Drawable.notification_bg_normal);
                
                this.SetBackgroundDrawable(drawable);
            }
        }

        public override void Draw(Canvas canvas)
        {
            base.Draw(canvas);
        }
    }
}