﻿using System;

using Android.App;
using Android.Content.PM;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;

using Microsoft.WindowsAzure.MobileServices;

using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using Plugin.CurrentActivity;
using Plugin.Permissions;

using System.IO;
using System.Threading.Tasks;
using Refractored.XamForms.PullToRefresh.Droid;

namespace Easy_Bid.Droid
{
    [Activity(Label = "Easy_Bid", Icon = "@drawable/icon", MainLauncher = false,
        ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation,
        Theme = "@style/MyTheme")]
    public class MainActivity : FormsApplicationActivity
    {
        internal static MainActivity Instance { get; private set; }
        protected override void OnCreate(Bundle bundle)
        {
            Forms.SetFlags("CollectionView_Experimental");

            base.OnCreate(bundle);

            Instance = this;

            CrossCurrentActivity.Current.Init(this, bundle);

            // Initialize Azure Mobile Apps
            CurrentPlatform.Init();

            // Initialize Xamarin Forms
            Forms.Init(this, bundle);

            PullToRefreshLayoutRenderer.Init();

            var width = Resources.DisplayMetrics.WidthPixels;
            var height = Resources.DisplayMetrics.HeightPixels;
            var density = Resources.DisplayMetrics.Density;

            App.ScreenWidth = (width - 0.5f) / density;
            App.ScreenHeight = (height - 0.5f) / density;

            var notificationManager = NotificationManager.FromContext(this);
            notificationManager.CancelAll();

            // Load the main application
            LoadApplication(new App());
        }
    }
}

